import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ca.umontreal.adt.list.FavoritesList;
import ca.umontreal.pqueues.SortedPriorityQueue;


public class MuzStream {

	public static void main(String[] args) {
		String request = "";
		int playlistCapacity = -1, playlistLimit = -1, numberOfFillings = -1, k = -1;
		try {
            request = args[0];
            playlistCapacity = Integer.parseInt(args[1]);
            playlistLimit = Integer.parseInt(args[2]);
            numberOfFillings = Integer.parseInt(args[3]);
            k = Integer.parseInt(args[4]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Arguments invalides.");
			System.exit(1);
		} catch (NumberFormatException e) {
	        System.err.println("Les 4 derniers arguments doivent etre des nombres.");
	        System.exit(1);
	    }
		
        System.out.println(request);
        System.out.println(String.format("%d", playlistCapacity));
        System.out.println(String.format("%d", playlistLimit));
        System.out.println(String.format("%d", numberOfFillings));
        System.out.println(String.format("%d", k));
        
        ArrayList<Track> trackList = new ArrayList<>();
		TSVTrackReader tracks = new TSVTrackReader(String.format("../data/%s.tsv", request), trackList);
		
		int n = 3;// charge n track en comptant les doublons
		int m = 6; // on fait jouer les m premiere tracks
		//int k = 3; // on choisit le top k track
		FavoritesList<Track> pickup = new FavoritesList<>(); //utiliser pour le top k
		ArrayList<Track> playList = new ArrayList<>();// toute le programme est dans cet array
		
		System.out.println("Initial Playlist:");
		for (int i = 0; i < n; i++) { //faudrait un while pour egaler le nombre dfe piece requise au debut...

			Track t = trackList.get(i);
			//on va boucler chaque chanson pour voir si elle est deja dans la liste.
			for (int l = 0; l < playList.size(); l++) {

				if (t.equals(playList.get(l))) { //si la chanson est deja dans larray, rank -= 1

					//reduce rank here
					//playList.get(l).setRank(String.valueOf(Integer.valueOf(playList.get(l).getRank())-1));
					playList.get(l).setRank(playList.get(l).getRank()-1);
					//sortedMap.put(playList.get(l).getRank(),t);
					//sortedMap.get(l).setRank(playList.get(l).getRank()-1);
					t = null; //si on a deja la chanson dans la playlist, on remplace t par null

					break;
				}

			}
			if ( t == null) { //si la chanson est null, cest pcq elle est deja dans la playlist et on skip lajout
				continue;
			}
			else {  //si cest pas nulle, on ajoute la track dans la playlist

				playList.add(t);
				//sortedMap.put(i,t);
				//treeMap.put();

				pickup.access(t);
			}

		}
		System.out.println("test1unsorted " + playList);


		//On trie larray en ordre decroissant
		Collections.sort(playList, new Comparator<Track>() {
			@Override
			public int compare(Track o1, Track o2) {
				return o1.compareTo(o2);
			}
		});
		System.out.println("test1sorted " + playList);


		//on fait jouer les chansons
	for( int i = 0; i < playList.size(); i++ ) {

	    //pickup.access( playList.get( i ) );
	    System.out.println( "playing song #" + i + ": " + playList.get( i ).getTitle() +" rank: " + playList.get(i).getRank() );

	}
	}

}
