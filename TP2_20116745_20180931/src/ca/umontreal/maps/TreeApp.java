package ca.umontreal.maps;

import java.util.concurrent.TimeUnit;
import ca.umontreal.adt.list.Position;

public class TreeApp {

    public static void main( String[] args ) {

	TreeMap<Integer,Integer> tm = new TreeMap<>();
	System.out.println( tm.size() );
	System.out.println( tm );
	tm.put( 6, 6 );
	System.out.println( tm.size() );
	System.out.println( tm );
	tm.put( 2, 2 );
	System.out.println( tm.size() );
	System.out.println( tm );
	tm.put( 1, 1 );
	System.out.println( tm );
	tm.put( 4, 4 );
	System.out.println( tm );
	tm.put( 9, 9 );
	System.out.println( tm );
	tm.put( 8, 8 );
	System.out.println( tm );
	for( Entry<Integer,Integer> entry : tm.subMap( 1, 10 ) )
	    System.out.println( entry );
    }
}
